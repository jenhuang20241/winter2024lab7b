
public enum Suit {

	HEARTS(0.4) {
		public String toString() {
			return "\u001B[31m"+"HEARTS"+"\u001B[0m";
		}
	},
	DIAMONDS(0.3) {
		public String toString() {
			return "\u001B[31m"+"DIAMONDS"+"\u001B[0m";
		}
	},
	CLUBS(0.2),
	SPADES(0.1);
	
	private double score;
	
	Suit (double score) {
		this.score = score;
	}
	
	public double getScore(){
		return this.score;
	}
}