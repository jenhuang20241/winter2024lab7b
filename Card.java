
public class Card
{
	private Suit cardSuit;
	private Value cardValue;
	
	//constructor
	public Card (Suit cardSuit, Value cardValue)
	{
		this.cardSuit = cardSuit;
		this.cardValue = cardValue;
	}

	//get methods
	public Suit getCardSuit()
	{
		return this.cardSuit;
	}
	public Value getCardValue()
	{
		return this.cardValue;
	}
	
	//toString method
	public String toString()
	{
		return this.cardValue + " of " + this.cardSuit;
	}
	
	//custom method to calculate the worth of each card
	public double calculateScore()
	{
		double score = cardSuit.getScore() + cardValue.getScore();
		
		return score;
	}
}