/* Author: Jennifer Huang ID: 1437051
* Game of War where 2 players draw a card
* whoever has the higher card wins a point
* winner is the player who cumulates the most points
*/

public class SimpleWar 
{
	public static void main (String[] args) 
	{ 
		Deck drawPile = new Deck();
		drawPile.shuffle();
		
		int player1Pts = 0;
		int player2Pts = 0;
		
		//displays players' inital scores
		System.out.println("Player 1 Score: " + player1Pts);
		System.out.println("Player 2 Score: " + player2Pts);
		System.out.println("------START GAME OF WAR-------");
		
		//loops through each round until there are no cards
		while (drawPile.length() > 1)
		{
			Card card1 = drawPile.drawTopCard();
			Card card2 = drawPile.drawTopCard();
			
			System.out.println("Player 1 Card: " + card1);
			System.out.println(card1.calculateScore());
			
			System.out.println("Player 2 Card: " + card2);
			System.out.println(card2.calculateScore());
			
			
			if (card1.calculateScore() > card2.calculateScore()) {
				System.out.println("Player 1 wins!");
				player1Pts++;
			
			} else {
				System.out.println("Player 2 wins!");
				player2Pts++;
			}
			
			System.out.println("Player 1 Score: " + player1Pts);
			System.out.println("Player 2 Score: " + player2Pts);
			System.out.println("------------------------------");
		}
		
		//displays the winner of the game
		System.out.println("-----------RESULTS------------");
		
		if (player1Pts == player2Pts) {
			System.out.println("It's a tie!");
		
		} else if (player1Pts > player2Pts) {
			System.out.println("Player 1 wins the game!!!");
			
		} else {
			System.out.println("Player 2 wins the game!!!");
		}
	}
}