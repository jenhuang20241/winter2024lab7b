import java.util.Random;

public class Deck 
{
	private Card[] cardsDeck;
	private int numberOfCards;
	private Random rng;
	
	//constructor: initializes the deck
	public Deck()
	{
		this.numberOfCards = 52;
		this.cardsDeck = new Card[numberOfCards];
		this.rng = new Random();
		
		int deckIndex = 0;
		for(Suit cardSuit: Suit.values())
		{
			for (Value cardValue: Value.values())
			{
				this.cardsDeck[deckIndex] = new Card(cardSuit, cardValue);
				deckIndex++;
			}
		}
	}
	
	
	//length()
	public int length()
	{
		return this.numberOfCards;
	}
	
	
	//drawTopCard()
	public Card drawTopCard()
	{
		Card lastCard = cardsDeck[numberOfCards-1];
		this.numberOfCards--;
		return lastCard;
	}
	
	
	//toString
	public String toString()
	{
		String result = "";
		
		for(int i=0; i<numberOfCards; i++)
		{
			result += this.cardsDeck[i] + "\n";
		}
		return result;
	}
	
	
	//shuffle()
	public void shuffle()
	{
		for (int i=0; i<numberOfCards; i++)
		{
			int randomIdx = rng.nextInt(this.numberOfCards - 1);
			Card temp = cardsDeck[i];
			cardsDeck[i] = cardsDeck[randomIdx];
			cardsDeck[randomIdx] = temp;
		}
	}
}